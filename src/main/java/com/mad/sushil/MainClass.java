package com.mad.sushil;

import com.mad.sushil.service.ResultStreamService;
import com.mad.sushil.serviceImpl.TestResultStreamService;

public class MainClass {
	public static void main(String[] args) {
		//JunitParent parentObj = new JunitParent();
		
		TestResultStreamService testResultStreamServiceObj = new TestResultStreamService();
		testResultStreamServiceObj.onNewResult("excutionId786");
		System.out.println("*********************************************************"+testResultStreamServiceObj.str);
		ResultStreamService resultStreamServiceObj = new TestResultStreamService();
		resultStreamServiceObj.streamResult("sushil2-personal call");
		resultStreamServiceObj.onNewResult("sushil2");
		System.out.println("*********************************************************"+resultStreamServiceObj.str);
	}

}
