package com.mad.sushil.service;

public abstract class ResultStreamService implements AsynchronousExecutionListner {

	public String str = "StringResultStreamService";

	@Override
	public void onNewResult(String executionId) {
		System.out.println("inside ResultStreamService onNewResult method "+executionId);
		streamResult(executionId);
	}

	public abstract void streamResult(String str);

}
