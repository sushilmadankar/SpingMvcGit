package com.mad.sushil.serviceImpl;

import com.mad.sushil.service.ResultStreamService;

public class TestResultStreamService extends ResultStreamService {

	public String str = "StringTestResultStreamService";

	@Override
	public void streamResult(String str) {
		System.out.println("inside TestResultStreamService streamResult method " + str);

	}

}
