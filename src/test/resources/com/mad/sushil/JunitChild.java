package com.mad.sushil;

import org.junit.Test;

import junit.framework.Assert;

public class JunitChild extends JunitParent {
	protected String name="child";

	public String readMe() {
		System.out.println(this.name);
		return this.name;
	}
	
	/*@Test
	public void testMeChild() {
		JunitChild childObj = new JunitChild();
		Assert.assertEquals("child", childObj.readMe());
	}*/
}
