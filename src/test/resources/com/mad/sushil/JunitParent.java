package com.mad.sushil;

import org.junit.Test;

public class JunitParent {

	protected String name="parent";
	
	public String readMe() {
		System.out.println(this.name);
		return this.name;
	}

	@Test
	public void testMeParent() {
		//JunitParent childObj = new JunitParent();
		//Assert.assertEquals("child", childObj.readMe());
		System.out.println(this.name);
	}
}
